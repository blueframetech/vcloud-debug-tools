import json

import sys
sys.path.insert(0, '../../cms-client-sdk-python')

import volar

def get_m3u8s(page):
  list = []
  for b in page['broadcasts']:
    list.append(b['m3u8_url'])
  return list

if len(sys.argv) < 3:
    print "usage:"
    print "python m3u8-grabber.py API_USER_API_KEY API_USER_SECRET DOMAIN SITE_SLUG"
    print "(e.g. python m3u8-grabber.py my_key my_secret vcloud.blueframetech.com my_site)"
    sys.exit()

API_USER_API_KEY = sys.argv[1]
API_USER_SECRET = sys.argv[2]
DOMAIN = sys.argv[3]
SITE_SLUG = sys.argv[4]

v = volar.Volar(API_USER_API_KEY, API_USER_SECRET, DOMAIN)

page = v.broadcasts({"include_m3u8":"true","list":"archived","site":SITE_SLUG})

if page == False:
  print "Unable to list first page of broadcasts. ", v.error
  sys.stdout.flush()

list = get_m3u8s(page)

numPagesPlusOne = page['num_pages'] + 1

for i in range(2,numPagesPlusOne):
  print ".", # no newline, show progress
  sys.stdout.flush()
  page = v.broadcasts({"include_m3u8":"true","list":"archived","site":SITE_SLUG,"page":i})
  if page == False:
    print "" # add newline
    print "Unable to list page " + i + " of broadcasts. ", v.error
    sys.stdout.flush()
  else:
    list += get_m3u8s(page)

print "done" # show that we're done, add newline
sys.stdout.flush()

print json.dumps(list, indent=4)
